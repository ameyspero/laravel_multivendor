<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Users
    Route::apiResource('users', 'UsersApiController');

    // Product Categories
    Route::post('product-categories/media', 'ProductCategoryApiController@storeMedia')->name('product-categories.storeMedia');
    Route::apiResource('product-categories', 'ProductCategoryApiController');

    // Product Tags
    Route::apiResource('product-tags', 'ProductTagApiController');

    // Products
    Route::post('products/media', 'ProductApiController@storeMedia')->name('products.storeMedia');
    Route::apiResource('products', 'ProductApiController');

    // Content Categories
    Route::apiResource('content-categories', 'ContentCategoryApiController');

    // Content Tags
    Route::apiResource('content-tags', 'ContentTagApiController');

    // Content Pages
    Route::post('content-pages/media', 'ContentPageApiController@storeMedia')->name('content-pages.storeMedia');
    Route::apiResource('content-pages', 'ContentPageApiController');

    // Menus
    Route::apiResource('menus', 'MenuApiController');

    // Orders
    Route::apiResource('orders', 'OrderApiController', ['except' => ['store']]);

    // Carts
    Route::apiResource('carts', 'CartApiController', ['except' => ['store']]);

    // Types
    Route::apiResource('types', 'TypeApiController');

    // Attributes
    Route::apiResource('attributes', 'AttributeApiController');

    // Product Languages
    Route::post('product-languages/media', 'ProductLanguageApiController@storeMedia')->name('product-languages.storeMedia');
    Route::apiResource('product-languages', 'ProductLanguageApiController');

    // Sale Offers
    Route::apiResource('sale-offers', 'SaleOffersApiController');

    // Taxes
    Route::apiResource('taxes', 'TaxApiController');

    // Product Seos
    Route::apiResource('product-seos', 'ProductSeoApiController');

    // Tickets
    Route::post('tickets/media', 'TicketApiController@storeMedia')->name('tickets.storeMedia');
    Route::apiResource('tickets', 'TicketApiController');

    // Ticket Messages
    Route::post('ticket-messages/media', 'TicketMessageApiController@storeMedia')->name('ticket-messages.storeMedia');
    Route::apiResource('ticket-messages', 'TicketMessageApiController');

    // Ratings
    Route::apiResource('ratings', 'RatingApiController');

    // Shipments
    Route::post('shipments/media', 'ShipmentApiController@storeMedia')->name('shipments.storeMedia');
    Route::apiResource('shipments', 'ShipmentApiController');

    // Payment Gateways
    Route::apiResource('payment-gateways', 'PaymentGatewayApiController');

    // Payment Transactions
    Route::apiResource('payment-transactions', 'PaymentTransactionApiController');

    // Refunds
    Route::apiResource('refunds', 'RefundApiController');

    // Settings
    Route::post('settings/media', 'SettingApiController@storeMedia')->name('settings.storeMedia');
    Route::apiResource('settings', 'SettingApiController');

    // Vouchers
    Route::apiResource('vouchers', 'VoucherApiController');

    // Wishlists
    Route::apiResource('wishlists', 'WishlistApiController');

    // Stores
    Route::post('stores/media', 'StoreApiController@storeMedia')->name('stores.storeMedia');
    Route::apiResource('stores', 'StoreApiController');

    // Approvals
    Route::apiResource('approvals', 'ApprovalApiController');
});
