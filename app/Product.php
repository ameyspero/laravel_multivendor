<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use \DateTimeInterface;

class Product extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'products';

    protected $appends = [
        'photo',
    ];

    public static $searchable = [
        'name',
        'price',
    ];

    const STATUS_SELECT = [
        '0' => 'disabled',
        '1' => 'enabled',
    ];

    protected $dates = [
        'expire_on',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const PRODUCT_TYPE_SELECT = [
        '0' => 'Normal Product',
        '1' => 'Downloadable',
    ];

    protected $fillable = [
        'name',
        'description',
        'price',
        'status',
        'language_id',
        'product_type',
        'stock_items',
        'expire_on',
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function categories()
    {
        return $this->belongsToMany(ProductCategory::class);
    }

    public function tags()
    {
        return $this->belongsToMany(ProductTag::class);
    }

    public function getPhotoAttribute()
    {
        $files = $this->getMedia('photo');
        $files->each(function ($item) {
            $item->url       = $item->getUrl();
            $item->thumbnail = $item->getUrl('thumb');
        });

        return $files;
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id');
    }

    public function getExpireOnAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setExpireOnAttribute($value)
    {
        $this->attributes['expire_on'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
