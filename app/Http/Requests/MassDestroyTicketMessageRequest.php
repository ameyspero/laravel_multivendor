<?php

namespace App\Http\Requests;

use App\TicketMessage;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyTicketMessageRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ticket_message_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:ticket_messages,id',
        ];
    }
}
