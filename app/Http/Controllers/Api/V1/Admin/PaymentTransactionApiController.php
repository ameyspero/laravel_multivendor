<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePaymentTransactionRequest;
use App\Http\Requests\UpdatePaymentTransactionRequest;
use App\Http\Resources\Admin\PaymentTransactionResource;
use App\PaymentTransaction;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PaymentTransactionApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('payment_transaction_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PaymentTransactionResource(PaymentTransaction::with(['product'])->get());
    }

    public function store(StorePaymentTransactionRequest $request)
    {
        $paymentTransaction = PaymentTransaction::create($request->all());

        return (new PaymentTransactionResource($paymentTransaction))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(PaymentTransaction $paymentTransaction)
    {
        abort_if(Gate::denies('payment_transaction_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PaymentTransactionResource($paymentTransaction->load(['product']));
    }

    public function update(UpdatePaymentTransactionRequest $request, PaymentTransaction $paymentTransaction)
    {
        $paymentTransaction->update($request->all());

        return (new PaymentTransactionResource($paymentTransaction))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(PaymentTransaction $paymentTransaction)
    {
        abort_if(Gate::denies('payment_transaction_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $paymentTransaction->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
