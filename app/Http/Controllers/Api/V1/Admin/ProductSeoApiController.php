<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductSeoRequest;
use App\Http\Requests\UpdateProductSeoRequest;
use App\Http\Resources\Admin\ProductSeoResource;
use App\ProductSeo;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductSeoApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('product_seo_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ProductSeoResource(ProductSeo::with(['product', 'language'])->get());
    }

    public function store(StoreProductSeoRequest $request)
    {
        $productSeo = ProductSeo::create($request->all());

        return (new ProductSeoResource($productSeo))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(ProductSeo $productSeo)
    {
        abort_if(Gate::denies('product_seo_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ProductSeoResource($productSeo->load(['product', 'language']));
    }

    public function update(UpdateProductSeoRequest $request, ProductSeo $productSeo)
    {
        $productSeo->update($request->all());

        return (new ProductSeoResource($productSeo))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(ProductSeo $productSeo)
    {
        abort_if(Gate::denies('product_seo_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $productSeo->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
