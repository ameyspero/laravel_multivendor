<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyPaymentTransactionRequest;
use App\Http\Requests\StorePaymentTransactionRequest;
use App\Http\Requests\UpdatePaymentTransactionRequest;
use App\PaymentTransaction;
use App\Product;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PaymentTransactionController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('payment_transaction_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $paymentTransactions = PaymentTransaction::all();

        return view('admin.paymentTransactions.index', compact('paymentTransactions'));
    }

    public function create()
    {
        abort_if(Gate::denies('payment_transaction_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $products = Product::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.paymentTransactions.create', compact('products'));
    }

    public function store(StorePaymentTransactionRequest $request)
    {
        $paymentTransaction = PaymentTransaction::create($request->all());

        return redirect()->route('admin.payment-transactions.index');
    }

    public function edit(PaymentTransaction $paymentTransaction)
    {
        abort_if(Gate::denies('payment_transaction_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $products = Product::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $paymentTransaction->load('product');

        return view('admin.paymentTransactions.edit', compact('products', 'paymentTransaction'));
    }

    public function update(UpdatePaymentTransactionRequest $request, PaymentTransaction $paymentTransaction)
    {
        $paymentTransaction->update($request->all());

        return redirect()->route('admin.payment-transactions.index');
    }

    public function show(PaymentTransaction $paymentTransaction)
    {
        abort_if(Gate::denies('payment_transaction_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $paymentTransaction->load('product');

        return view('admin.paymentTransactions.show', compact('paymentTransaction'));
    }

    public function destroy(PaymentTransaction $paymentTransaction)
    {
        abort_if(Gate::denies('payment_transaction_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $paymentTransaction->delete();

        return back();
    }

    public function massDestroy(MassDestroyPaymentTransactionRequest $request)
    {
        PaymentTransaction::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
