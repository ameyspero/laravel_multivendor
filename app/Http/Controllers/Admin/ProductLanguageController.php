<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyProductLanguageRequest;
use App\Http\Requests\StoreProductLanguageRequest;
use App\Http\Requests\UpdateProductLanguageRequest;
use App\Language;
use App\Product;
use App\ProductLanguage;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class ProductLanguageController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('product_language_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $productLanguages = ProductLanguage::all();

        return view('admin.productLanguages.index', compact('productLanguages'));
    }

    public function create()
    {
        abort_if(Gate::denies('product_language_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $products = Product::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $languages = Language::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.productLanguages.create', compact('products', 'languages'));
    }

    public function store(StoreProductLanguageRequest $request)
    {
        $productLanguage = ProductLanguage::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $productLanguage->id]);
        }

        return redirect()->route('admin.product-languages.index');
    }

    public function edit(ProductLanguage $productLanguage)
    {
        abort_if(Gate::denies('product_language_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $products = Product::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $languages = Language::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        $productLanguage->load('product', 'language');

        return view('admin.productLanguages.edit', compact('products', 'languages', 'productLanguage'));
    }

    public function update(UpdateProductLanguageRequest $request, ProductLanguage $productLanguage)
    {
        $productLanguage->update($request->all());

        return redirect()->route('admin.product-languages.index');
    }

    public function show(ProductLanguage $productLanguage)
    {
        abort_if(Gate::denies('product_language_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $productLanguage->load('product', 'language');

        return view('admin.productLanguages.show', compact('productLanguage'));
    }

    public function destroy(ProductLanguage $productLanguage)
    {
        abort_if(Gate::denies('product_language_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $productLanguage->delete();

        return back();
    }

    public function massDestroy(MassDestroyProductLanguageRequest $request)
    {
        ProductLanguage::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('product_language_create') && Gate::denies('product_language_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new ProductLanguage();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
