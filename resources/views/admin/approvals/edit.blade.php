@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.approval.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.approvals.update", [$approval->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="app">{{ trans('cruds.approval.fields.app') }}</label>
                <input class="form-control {{ $errors->has('app') ? 'is-invalid' : '' }}" type="text" name="app" id="app" value="{{ old('app', $approval->app) }}">
                @if($errors->has('app'))
                    <span class="text-danger">{{ $errors->first('app') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.approval.fields.app_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection