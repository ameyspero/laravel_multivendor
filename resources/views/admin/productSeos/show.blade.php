@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.productSeo.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.product-seos.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.productSeo.fields.id') }}
                        </th>
                        <td>
                            {{ $productSeo->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.productSeo.fields.product') }}
                        </th>
                        <td>
                            {{ $productSeo->product->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.productSeo.fields.page_title') }}
                        </th>
                        <td>
                            {{ $productSeo->page_title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.productSeo.fields.meta_description') }}
                        </th>
                        <td>
                            {{ $productSeo->meta_description }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.productSeo.fields.meta_keywords') }}
                        </th>
                        <td>
                            {{ $productSeo->meta_keywords }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.productSeo.fields.language') }}
                        </th>
                        <td>
                            {{ $productSeo->language->title ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.product-seos.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection