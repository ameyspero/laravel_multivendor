@extends('layouts.admin')
@section('content')
@can('product_seo_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.product-seos.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.productSeo.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.productSeo.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-ProductSeo">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.productSeo.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.productSeo.fields.product') }}
                        </th>
                        <th>
                            {{ trans('cruds.productSeo.fields.page_title') }}
                        </th>
                        <th>
                            {{ trans('cruds.productSeo.fields.meta_description') }}
                        </th>
                        <th>
                            {{ trans('cruds.productSeo.fields.meta_keywords') }}
                        </th>
                        <th>
                            {{ trans('cruds.productSeo.fields.language') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($productSeos as $key => $productSeo)
                        <tr data-entry-id="{{ $productSeo->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $productSeo->id ?? '' }}
                            </td>
                            <td>
                                {{ $productSeo->product->name ?? '' }}
                            </td>
                            <td>
                                {{ $productSeo->page_title ?? '' }}
                            </td>
                            <td>
                                {{ $productSeo->meta_description ?? '' }}
                            </td>
                            <td>
                                {{ $productSeo->meta_keywords ?? '' }}
                            </td>
                            <td>
                                {{ $productSeo->language->title ?? '' }}
                            </td>
                            <td>
                                @can('product_seo_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.product-seos.show', $productSeo->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('product_seo_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.product-seos.edit', $productSeo->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('product_seo_delete')
                                    <form action="{{ route('admin.product-seos.destroy', $productSeo->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('product_seo_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.product-seos.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-ProductSeo:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection