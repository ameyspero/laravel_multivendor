@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.paymentTransaction.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.payment-transactions.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="product_id">{{ trans('cruds.paymentTransaction.fields.product') }}</label>
                <select class="form-control select2 {{ $errors->has('product') ? 'is-invalid' : '' }}" name="product_id" id="product_id">
                    @foreach($products as $id => $product)
                        <option value="{{ $id }}" {{ old('product_id') == $id ? 'selected' : '' }}>{{ $product }}</option>
                    @endforeach
                </select>
                @if($errors->has('product'))
                    <span class="text-danger">{{ $errors->first('product') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.paymentTransaction.fields.product_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="transaction">{{ trans('cruds.paymentTransaction.fields.transaction') }}</label>
                <input class="form-control {{ $errors->has('transaction') ? 'is-invalid' : '' }}" type="text" name="transaction" id="transaction" value="{{ old('transaction', '') }}">
                @if($errors->has('transaction'))
                    <span class="text-danger">{{ $errors->first('transaction') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.paymentTransaction.fields.transaction_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="gateway_response">{{ trans('cruds.paymentTransaction.fields.gateway_response') }}</label>
                <input class="form-control {{ $errors->has('gateway_response') ? 'is-invalid' : '' }}" type="text" name="gateway_response" id="gateway_response" value="{{ old('gateway_response', '') }}">
                @if($errors->has('gateway_response'))
                    <span class="text-danger">{{ $errors->first('gateway_response') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.paymentTransaction.fields.gateway_response_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection