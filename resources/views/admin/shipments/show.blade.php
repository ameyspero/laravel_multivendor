@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.shipment.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.shipments.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.shipment.fields.id') }}
                        </th>
                        <td>
                            {{ $shipment->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.shipment.fields.name') }}
                        </th>
                        <td>
                            {{ $shipment->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.shipment.fields.fee') }}
                        </th>
                        <td>
                            {{ $shipment->fee }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.shipment.fields.description') }}
                        </th>
                        <td>
                            {!! $shipment->description !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.shipment.fields.language') }}
                        </th>
                        <td>
                            {{ $shipment->language->title ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.shipments.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection