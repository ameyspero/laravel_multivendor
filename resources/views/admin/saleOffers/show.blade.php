@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.saleOffer.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.sale-offers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.saleOffer.fields.id') }}
                        </th>
                        <td>
                            {{ $saleOffer->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.saleOffer.fields.product') }}
                        </th>
                        <td>
                            {{ $saleOffer->product->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.saleOffer.fields.started_at') }}
                        </th>
                        <td>
                            {{ $saleOffer->started_at }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.saleOffer.fields.ended_at') }}
                        </th>
                        <td>
                            {{ $saleOffer->ended_at }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.saleOffer.fields.title') }}
                        </th>
                        <td>
                            {{ $saleOffer->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.saleOffer.fields.discount_percentage') }}
                        </th>
                        <td>
                            {{ $saleOffer->discount_percentage }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.sale-offers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection