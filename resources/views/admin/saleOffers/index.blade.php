@extends('layouts.admin')
@section('content')
@can('sale_offer_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.sale-offers.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.saleOffer.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.saleOffer.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-SaleOffer">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.saleOffer.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.saleOffer.fields.product') }}
                        </th>
                        <th>
                            {{ trans('cruds.saleOffer.fields.started_at') }}
                        </th>
                        <th>
                            {{ trans('cruds.saleOffer.fields.ended_at') }}
                        </th>
                        <th>
                            {{ trans('cruds.saleOffer.fields.title') }}
                        </th>
                        <th>
                            {{ trans('cruds.saleOffer.fields.discount_percentage') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($saleOffers as $key => $saleOffer)
                        <tr data-entry-id="{{ $saleOffer->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $saleOffer->id ?? '' }}
                            </td>
                            <td>
                                {{ $saleOffer->product->name ?? '' }}
                            </td>
                            <td>
                                {{ $saleOffer->started_at ?? '' }}
                            </td>
                            <td>
                                {{ $saleOffer->ended_at ?? '' }}
                            </td>
                            <td>
                                {{ $saleOffer->title ?? '' }}
                            </td>
                            <td>
                                {{ $saleOffer->discount_percentage ?? '' }}
                            </td>
                            <td>
                                @can('sale_offer_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.sale-offers.show', $saleOffer->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('sale_offer_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.sale-offers.edit', $saleOffer->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('sale_offer_delete')
                                    <form action="{{ route('admin.sale-offers.destroy', $saleOffer->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('sale_offer_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.sale-offers.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-SaleOffer:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection