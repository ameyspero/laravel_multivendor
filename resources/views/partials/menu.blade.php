<aside class="main-sidebar sidebar-dark-primary elevation-4" style="min-height: 917px;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <span class="brand-text font-weight-light">{{ trans('panel.site_title') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li>
                    <select class="searchable-field form-control">

                    </select>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route("admin.home") }}">
                        <i class="fas fa-fw fa-tachometer-alt nav-icon">
                        </i>
                        <p>
                            {{ trans('global.dashboard') }}
                        </p>
                    </a>
                </li>
                @can('user_management_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/permissions*') ? 'menu-open' : '' }} {{ request()->is('admin/roles*') ? 'menu-open' : '' }} {{ request()->is('admin/users*') ? 'menu-open' : '' }} {{ request()->is('admin/audit-logs*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw nav-icon fas fa-users">

                            </i>
                            <p>
                                {{ trans('cruds.userManagement.title') }}
                                <i class="right fa fa-fw fa-angle-left nav-icon"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('permission_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-unlock-alt">

                                        </i>
                                        <p>
                                            {{ trans('cruds.permission.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('role_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-briefcase">

                                        </i>
                                        <p>
                                            {{ trans('cruds.role.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('user_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-user">

                                        </i>
                                        <p>
                                            {{ trans('cruds.user.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('audit_log_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.audit-logs.index") }}" class="nav-link {{ request()->is('admin/audit-logs') || request()->is('admin/audit-logs/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-file-alt">

                                        </i>
                                        <p>
                                            {{ trans('cruds.auditLog.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('product_management_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/product-categories*') ? 'menu-open' : '' }} {{ request()->is('admin/product-tags*') ? 'menu-open' : '' }} {{ request()->is('admin/products*') ? 'menu-open' : '' }} {{ request()->is('admin/tests*') ? 'menu-open' : '' }} {{ request()->is('admin/types*') ? 'menu-open' : '' }} {{ request()->is('admin/attributes*') ? 'menu-open' : '' }} {{ request()->is('admin/product-languages*') ? 'menu-open' : '' }} {{ request()->is('admin/wishlists*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw nav-icon fas fa-shopping-cart">

                            </i>
                            <p>
                                {{ trans('cruds.productManagement.title') }}
                                <i class="right fa fa-fw fa-angle-left nav-icon"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('product_category_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.product-categories.index") }}" class="nav-link {{ request()->is('admin/product-categories') || request()->is('admin/product-categories/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-folder">

                                        </i>
                                        <p>
                                            {{ trans('cruds.productCategory.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('product_tag_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.product-tags.index") }}" class="nav-link {{ request()->is('admin/product-tags') || request()->is('admin/product-tags/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-folder">

                                        </i>
                                        <p>
                                            {{ trans('cruds.productTag.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('product_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.products.index") }}" class="nav-link {{ request()->is('admin/products') || request()->is('admin/products/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-shopping-cart">

                                        </i>
                                        <p>
                                            {{ trans('cruds.product.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('type_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.types.index") }}" class="nav-link {{ request()->is('admin/types') || request()->is('admin/types/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-link">

                                        </i>
                                        <p>
                                            {{ trans('cruds.type.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('attribute_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.attributes.index") }}" class="nav-link {{ request()->is('admin/attributes') || request()->is('admin/attributes/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fab fa-audible">

                                        </i>
                                        <p>
                                            {{ trans('cruds.attribute.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('product_language_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.product-languages.index") }}" class="nav-link {{ request()->is('admin/product-languages') || request()->is('admin/product-languages/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-globe-asia">

                                        </i>
                                        <p>
                                            {{ trans('cruds.productLanguage.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('wishlist_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.wishlists.index") }}" class="nav-link {{ request()->is('admin/wishlists') || request()->is('admin/wishlists/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-heart">

                                        </i>
                                        <p>
                                            {{ trans('cruds.wishlist.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('user_alert_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.user-alerts.index") }}" class="nav-link {{ request()->is('admin/user-alerts') || request()->is('admin/user-alerts/*') ? 'active' : '' }}">
                            <i class="fa-fw nav-icon fas fa-bell">

                            </i>
                            <p>
                                {{ trans('cruds.userAlert.title') }}
                            </p>
                        </a>
                    </li>
                @endcan
                @can('content_management_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/content-categories*') ? 'menu-open' : '' }} {{ request()->is('admin/content-tags*') ? 'menu-open' : '' }} {{ request()->is('admin/content-pages*') ? 'menu-open' : '' }} {{ request()->is('admin/menus*') ? 'menu-open' : '' }} {{ request()->is('admin/product-seos*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw nav-icon fas fa-book">

                            </i>
                            <p>
                                {{ trans('cruds.contentManagement.title') }}
                                <i class="right fa fa-fw fa-angle-left nav-icon"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('content_category_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.content-categories.index") }}" class="nav-link {{ request()->is('admin/content-categories') || request()->is('admin/content-categories/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-folder">

                                        </i>
                                        <p>
                                            {{ trans('cruds.contentCategory.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('content_tag_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.content-tags.index") }}" class="nav-link {{ request()->is('admin/content-tags') || request()->is('admin/content-tags/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-tags">

                                        </i>
                                        <p>
                                            {{ trans('cruds.contentTag.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('content_page_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.content-pages.index") }}" class="nav-link {{ request()->is('admin/content-pages') || request()->is('admin/content-pages/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-file">

                                        </i>
                                        <p>
                                            {{ trans('cruds.contentPage.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('menu_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.menus.index") }}" class="nav-link {{ request()->is('admin/menus') || request()->is('admin/menus/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-align-justify">

                                        </i>
                                        <p>
                                            {{ trans('cruds.menu.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('product_seo_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.product-seos.index") }}" class="nav-link {{ request()->is('admin/product-seos') || request()->is('admin/product-seos/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fab fa-google">

                                        </i>
                                        <p>
                                            {{ trans('cruds.productSeo.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('language_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.languages.index") }}" class="nav-link {{ request()->is('admin/languages') || request()->is('admin/languages/*') ? 'active' : '' }}">
                            <i class="fa-fw nav-icon fas fa-language">

                            </i>
                            <p>
                                {{ trans('cruds.language.title') }}
                            </p>
                        </a>
                    </li>
                @endcan
                @can('card_and_payment_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/orders*') ? 'menu-open' : '' }} {{ request()->is('admin/carts*') ? 'menu-open' : '' }} {{ request()->is('admin/taxes*') ? 'menu-open' : '' }} {{ request()->is('admin/payment-gateways*') ? 'menu-open' : '' }} {{ request()->is('admin/payment-transactions*') ? 'menu-open' : '' }} {{ request()->is('admin/refunds*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw nav-icon far fa-credit-card">

                            </i>
                            <p>
                                {{ trans('cruds.cardAndPayment.title') }}
                                <i class="right fa fa-fw fa-angle-left nav-icon"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('order_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.orders.index") }}" class="nav-link {{ request()->is('admin/orders') || request()->is('admin/orders/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon far fa-money-bill-alt">

                                        </i>
                                        <p>
                                            {{ trans('cruds.order.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('cart_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.carts.index") }}" class="nav-link {{ request()->is('admin/carts') || request()->is('admin/carts/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-shopping-cart">

                                        </i>
                                        <p>
                                            {{ trans('cruds.cart.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('tax_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.taxes.index") }}" class="nav-link {{ request()->is('admin/taxes') || request()->is('admin/taxes/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-briefcase">

                                        </i>
                                        <p>
                                            {{ trans('cruds.tax.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('payment_gateway_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.payment-gateways.index") }}" class="nav-link {{ request()->is('admin/payment-gateways') || request()->is('admin/payment-gateways/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fab fa-cc-paypal">

                                        </i>
                                        <p>
                                            {{ trans('cruds.paymentGateway.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('payment_transaction_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.payment-transactions.index") }}" class="nav-link {{ request()->is('admin/payment-transactions') || request()->is('admin/payment-transactions/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-exchange-alt">

                                        </i>
                                        <p>
                                            {{ trans('cruds.paymentTransaction.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('refund_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.refunds.index") }}" class="nav-link {{ request()->is('admin/refunds') || request()->is('admin/refunds/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-donate">

                                        </i>
                                        <p>
                                            {{ trans('cruds.refund.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('sale_offer_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.sale-offers.index") }}" class="nav-link {{ request()->is('admin/sale-offers') || request()->is('admin/sale-offers/*') ? 'active' : '' }}">
                            <i class="fa-fw nav-icon far fa-handshake">

                            </i>
                            <p>
                                {{ trans('cruds.saleOffer.title') }}
                            </p>
                        </a>
                    </li>
                @endcan
                @can('customer_service_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/tickets*') ? 'menu-open' : '' }} {{ request()->is('admin/ticket-messages*') ? 'menu-open' : '' }} {{ request()->is('admin/ratings*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw nav-icon fas fa-hand-holding-heart">

                            </i>
                            <p>
                                {{ trans('cruds.customerService.title') }}
                                <i class="right fa fa-fw fa-angle-left nav-icon"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('ticket_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.tickets.index") }}" class="nav-link {{ request()->is('admin/tickets') || request()->is('admin/tickets/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-question-circle">

                                        </i>
                                        <p>
                                            {{ trans('cruds.ticket.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('ticket_message_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.ticket-messages.index") }}" class="nav-link {{ request()->is('admin/ticket-messages') || request()->is('admin/ticket-messages/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-envelope">

                                        </i>
                                        <p>
                                            {{ trans('cruds.ticketMessage.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('rating_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.ratings.index") }}" class="nav-link {{ request()->is('admin/ratings') || request()->is('admin/ratings/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-star-half-alt">

                                        </i>
                                        <p>
                                            {{ trans('cruds.rating.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('shipment_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.shipments.index") }}" class="nav-link {{ request()->is('admin/shipments') || request()->is('admin/shipments/*') ? 'active' : '' }}">
                            <i class="fa-fw nav-icon fas fa-shipping-fast">

                            </i>
                            <p>
                                {{ trans('cruds.shipment.title') }}
                            </p>
                        </a>
                    </li>
                @endcan
                @can('setting_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.settings.index") }}" class="nav-link {{ request()->is('admin/settings') || request()->is('admin/settings/*') ? 'active' : '' }}">
                            <i class="fa-fw nav-icon fas fa-cogs">

                            </i>
                            <p>
                                {{ trans('cruds.setting.title') }}
                            </p>
                        </a>
                    </li>
                @endcan
                @can('voucher_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.vouchers.index") }}" class="nav-link {{ request()->is('admin/vouchers') || request()->is('admin/vouchers/*') ? 'active' : '' }}">
                            <i class="fa-fw nav-icon fas fa-ticket-alt">

                            </i>
                            <p>
                                {{ trans('cruds.voucher.title') }}
                            </p>
                        </a>
                    </li>
                @endcan
                @can('multi_vendor_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/stores*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw nav-icon fas fa-store-alt">

                            </i>
                            <p>
                                {{ trans('cruds.multiVendor.title') }}
                                <i class="right fa fa-fw fa-angle-left nav-icon"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('store_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.stores.index") }}" class="nav-link {{ request()->is('admin/stores') || request()->is('admin/stores/*') ? 'active' : '' }}">
                                        <i class="fa-fw nav-icon fas fa-store">

                                        </i>
                                        <p>
                                            {{ trans('cruds.store.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('approval_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.approvals.index") }}" class="nav-link {{ request()->is('admin/approvals') || request()->is('admin/approvals/*') ? 'active' : '' }}">
                            <i class="fa-fw nav-icon fas fa-cogs">

                            </i>
                            <p>
                                {{ trans('cruds.approval.title') }}
                            </p>
                        </a>
                    </li>
                @endcan
                @php($unread = \App\QaTopic::unreadCount())
                    <li class="nav-item">
                        <a href="{{ route("admin.messenger.index") }}" class="{{ request()->is('admin/messenger') || request()->is('admin/messenger/*') ? 'active' : '' }} nav-link">
                            <i class="fa-fw fa fa-envelope nav-icon">

                            </i>
                            <p>{{ trans('global.messages') }}</p>
                            @if($unread > 0)
                                <strong>( {{ $unread }} )</strong>
                            @endif
                        </a>
                    </li>
                    @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
                        @can('profile_password_edit')
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('profile/password') || request()->is('profile/password/*') ? 'active' : '' }}" href="{{ route('profile.password.edit') }}">
                                    <i class="fa-fw fas fa-key nav-icon">
                                    </i>
                                    <p>
                                        {{ trans('global.change_password') }}
                                    </p>
                                </a>
                            </li>
                        @endcan
                    @endif
                    <li class="nav-item">
                        <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                            <p>
                                <i class="fas fa-fw fa-sign-out-alt nav-icon">

                                </i>
                                <p>{{ trans('global.logout') }}</p>
                            </p>
                        </a>
                    </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>