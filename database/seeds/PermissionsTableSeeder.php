<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => '1',
                'title' => 'user_management_access',
            ],
            [
                'id'    => '2',
                'title' => 'permission_create',
            ],
            [
                'id'    => '3',
                'title' => 'permission_edit',
            ],
            [
                'id'    => '4',
                'title' => 'permission_show',
            ],
            [
                'id'    => '5',
                'title' => 'permission_delete',
            ],
            [
                'id'    => '6',
                'title' => 'permission_access',
            ],
            [
                'id'    => '7',
                'title' => 'role_create',
            ],
            [
                'id'    => '8',
                'title' => 'role_edit',
            ],
            [
                'id'    => '9',
                'title' => 'role_show',
            ],
            [
                'id'    => '10',
                'title' => 'role_delete',
            ],
            [
                'id'    => '11',
                'title' => 'role_access',
            ],
            [
                'id'    => '12',
                'title' => 'user_create',
            ],
            [
                'id'    => '13',
                'title' => 'user_edit',
            ],
            [
                'id'    => '14',
                'title' => 'user_show',
            ],
            [
                'id'    => '15',
                'title' => 'user_delete',
            ],
            [
                'id'    => '16',
                'title' => 'user_access',
            ],
            [
                'id'    => '17',
                'title' => 'product_management_access',
            ],
            [
                'id'    => '18',
                'title' => 'product_category_create',
            ],
            [
                'id'    => '19',
                'title' => 'product_category_edit',
            ],
            [
                'id'    => '20',
                'title' => 'product_category_show',
            ],
            [
                'id'    => '21',
                'title' => 'product_category_delete',
            ],
            [
                'id'    => '22',
                'title' => 'product_category_access',
            ],
            [
                'id'    => '23',
                'title' => 'product_tag_create',
            ],
            [
                'id'    => '24',
                'title' => 'product_tag_edit',
            ],
            [
                'id'    => '25',
                'title' => 'product_tag_show',
            ],
            [
                'id'    => '26',
                'title' => 'product_tag_delete',
            ],
            [
                'id'    => '27',
                'title' => 'product_tag_access',
            ],
            [
                'id'    => '28',
                'title' => 'product_create',
            ],
            [
                'id'    => '29',
                'title' => 'product_edit',
            ],
            [
                'id'    => '30',
                'title' => 'product_show',
            ],
            [
                'id'    => '31',
                'title' => 'product_delete',
            ],
            [
                'id'    => '32',
                'title' => 'product_access',
            ],
            [
                'id'    => '33',
                'title' => 'audit_log_show',
            ],
            [
                'id'    => '34',
                'title' => 'audit_log_access',
            ],
            [
                'id'    => '35',
                'title' => 'user_alert_create',
            ],
            [
                'id'    => '36',
                'title' => 'user_alert_show',
            ],
            [
                'id'    => '37',
                'title' => 'user_alert_delete',
            ],
            [
                'id'    => '38',
                'title' => 'user_alert_access',
            ],
            [
                'id'    => '39',
                'title' => 'content_management_access',
            ],
            [
                'id'    => '40',
                'title' => 'content_category_create',
            ],
            [
                'id'    => '41',
                'title' => 'content_category_edit',
            ],
            [
                'id'    => '42',
                'title' => 'content_category_show',
            ],
            [
                'id'    => '43',
                'title' => 'content_category_delete',
            ],
            [
                'id'    => '44',
                'title' => 'content_category_access',
            ],
            [
                'id'    => '45',
                'title' => 'content_tag_create',
            ],
            [
                'id'    => '46',
                'title' => 'content_tag_edit',
            ],
            [
                'id'    => '47',
                'title' => 'content_tag_show',
            ],
            [
                'id'    => '48',
                'title' => 'content_tag_delete',
            ],
            [
                'id'    => '49',
                'title' => 'content_tag_access',
            ],
            [
                'id'    => '50',
                'title' => 'content_page_create',
            ],
            [
                'id'    => '51',
                'title' => 'content_page_edit',
            ],
            [
                'id'    => '52',
                'title' => 'content_page_show',
            ],
            [
                'id'    => '53',
                'title' => 'content_page_delete',
            ],
            [
                'id'    => '54',
                'title' => 'content_page_access',
            ],
            [
                'id'    => '55',
                'title' => 'test_access',
            ],
            [
                'id'    => '56',
                'title' => 'menu_create',
            ],
            [
                'id'    => '57',
                'title' => 'menu_edit',
            ],
            [
                'id'    => '58',
                'title' => 'menu_show',
            ],
            [
                'id'    => '59',
                'title' => 'menu_delete',
            ],
            [
                'id'    => '60',
                'title' => 'menu_access',
            ],
            [
                'id'    => '61',
                'title' => 'order_edit',
            ],
            [
                'id'    => '62',
                'title' => 'order_show',
            ],
            [
                'id'    => '63',
                'title' => 'order_delete',
            ],
            [
                'id'    => '64',
                'title' => 'order_access',
            ],
            [
                'id'    => '65',
                'title' => 'cart_edit',
            ],
            [
                'id'    => '66',
                'title' => 'cart_show',
            ],
            [
                'id'    => '67',
                'title' => 'cart_delete',
            ],
            [
                'id'    => '68',
                'title' => 'cart_access',
            ],
            [
                'id'    => '69',
                'title' => 'type_create',
            ],
            [
                'id'    => '70',
                'title' => 'type_edit',
            ],
            [
                'id'    => '71',
                'title' => 'type_show',
            ],
            [
                'id'    => '72',
                'title' => 'type_delete',
            ],
            [
                'id'    => '73',
                'title' => 'type_access',
            ],
            [
                'id'    => '74',
                'title' => 'attribute_create',
            ],
            [
                'id'    => '75',
                'title' => 'attribute_edit',
            ],
            [
                'id'    => '76',
                'title' => 'attribute_show',
            ],
            [
                'id'    => '77',
                'title' => 'attribute_delete',
            ],
            [
                'id'    => '78',
                'title' => 'attribute_access',
            ],
            [
                'id'    => '79',
                'title' => 'language_create',
            ],
            [
                'id'    => '80',
                'title' => 'language_edit',
            ],
            [
                'id'    => '81',
                'title' => 'language_show',
            ],
            [
                'id'    => '82',
                'title' => 'language_delete',
            ],
            [
                'id'    => '83',
                'title' => 'language_access',
            ],
            [
                'id'    => '84',
                'title' => 'card_and_payment_access',
            ],
            [
                'id'    => '85',
                'title' => 'product_language_create',
            ],
            [
                'id'    => '86',
                'title' => 'product_language_edit',
            ],
            [
                'id'    => '87',
                'title' => 'product_language_show',
            ],
            [
                'id'    => '88',
                'title' => 'product_language_delete',
            ],
            [
                'id'    => '89',
                'title' => 'product_language_access',
            ],
            [
                'id'    => '90',
                'title' => 'sale_offer_create',
            ],
            [
                'id'    => '91',
                'title' => 'sale_offer_edit',
            ],
            [
                'id'    => '92',
                'title' => 'sale_offer_show',
            ],
            [
                'id'    => '93',
                'title' => 'sale_offer_delete',
            ],
            [
                'id'    => '94',
                'title' => 'sale_offer_access',
            ],
            [
                'id'    => '95',
                'title' => 'tax_create',
            ],
            [
                'id'    => '96',
                'title' => 'tax_edit',
            ],
            [
                'id'    => '97',
                'title' => 'tax_show',
            ],
            [
                'id'    => '98',
                'title' => 'tax_delete',
            ],
            [
                'id'    => '99',
                'title' => 'tax_access',
            ],
            [
                'id'    => '100',
                'title' => 'product_seo_create',
            ],
            [
                'id'    => '101',
                'title' => 'product_seo_edit',
            ],
            [
                'id'    => '102',
                'title' => 'product_seo_show',
            ],
            [
                'id'    => '103',
                'title' => 'product_seo_delete',
            ],
            [
                'id'    => '104',
                'title' => 'product_seo_access',
            ],
            [
                'id'    => '105',
                'title' => 'ticket_create',
            ],
            [
                'id'    => '106',
                'title' => 'ticket_edit',
            ],
            [
                'id'    => '107',
                'title' => 'ticket_show',
            ],
            [
                'id'    => '108',
                'title' => 'ticket_delete',
            ],
            [
                'id'    => '109',
                'title' => 'ticket_access',
            ],
            [
                'id'    => '110',
                'title' => 'ticket_message_create',
            ],
            [
                'id'    => '111',
                'title' => 'ticket_message_edit',
            ],
            [
                'id'    => '112',
                'title' => 'ticket_message_show',
            ],
            [
                'id'    => '113',
                'title' => 'ticket_message_delete',
            ],
            [
                'id'    => '114',
                'title' => 'ticket_message_access',
            ],
            [
                'id'    => '115',
                'title' => 'customer_service_access',
            ],
            [
                'id'    => '116',
                'title' => 'rating_create',
            ],
            [
                'id'    => '117',
                'title' => 'rating_edit',
            ],
            [
                'id'    => '118',
                'title' => 'rating_show',
            ],
            [
                'id'    => '119',
                'title' => 'rating_delete',
            ],
            [
                'id'    => '120',
                'title' => 'rating_access',
            ],
            [
                'id'    => '121',
                'title' => 'shipment_create',
            ],
            [
                'id'    => '122',
                'title' => 'shipment_edit',
            ],
            [
                'id'    => '123',
                'title' => 'shipment_show',
            ],
            [
                'id'    => '124',
                'title' => 'shipment_delete',
            ],
            [
                'id'    => '125',
                'title' => 'shipment_access',
            ],
            [
                'id'    => '126',
                'title' => 'payment_gateway_create',
            ],
            [
                'id'    => '127',
                'title' => 'payment_gateway_edit',
            ],
            [
                'id'    => '128',
                'title' => 'payment_gateway_show',
            ],
            [
                'id'    => '129',
                'title' => 'payment_gateway_delete',
            ],
            [
                'id'    => '130',
                'title' => 'payment_gateway_access',
            ],
            [
                'id'    => '131',
                'title' => 'payment_transaction_create',
            ],
            [
                'id'    => '132',
                'title' => 'payment_transaction_edit',
            ],
            [
                'id'    => '133',
                'title' => 'payment_transaction_show',
            ],
            [
                'id'    => '134',
                'title' => 'payment_transaction_delete',
            ],
            [
                'id'    => '135',
                'title' => 'payment_transaction_access',
            ],
            [
                'id'    => '136',
                'title' => 'refund_create',
            ],
            [
                'id'    => '137',
                'title' => 'refund_edit',
            ],
            [
                'id'    => '138',
                'title' => 'refund_show',
            ],
            [
                'id'    => '139',
                'title' => 'refund_delete',
            ],
            [
                'id'    => '140',
                'title' => 'refund_access',
            ],
            [
                'id'    => '141',
                'title' => 'setting_create',
            ],
            [
                'id'    => '142',
                'title' => 'setting_edit',
            ],
            [
                'id'    => '143',
                'title' => 'setting_show',
            ],
            [
                'id'    => '144',
                'title' => 'setting_delete',
            ],
            [
                'id'    => '145',
                'title' => 'setting_access',
            ],
            [
                'id'    => '146',
                'title' => 'voucher_create',
            ],
            [
                'id'    => '147',
                'title' => 'voucher_edit',
            ],
            [
                'id'    => '148',
                'title' => 'voucher_show',
            ],
            [
                'id'    => '149',
                'title' => 'voucher_delete',
            ],
            [
                'id'    => '150',
                'title' => 'voucher_access',
            ],
            [
                'id'    => '151',
                'title' => 'wishlist_create',
            ],
            [
                'id'    => '152',
                'title' => 'wishlist_edit',
            ],
            [
                'id'    => '153',
                'title' => 'wishlist_show',
            ],
            [
                'id'    => '154',
                'title' => 'wishlist_delete',
            ],
            [
                'id'    => '155',
                'title' => 'wishlist_access',
            ],
            [
                'id'    => '156',
                'title' => 'multi_vendor_access',
            ],
            [
                'id'    => '157',
                'title' => 'store_create',
            ],
            [
                'id'    => '158',
                'title' => 'store_edit',
            ],
            [
                'id'    => '159',
                'title' => 'store_show',
            ],
            [
                'id'    => '160',
                'title' => 'store_delete',
            ],
            [
                'id'    => '161',
                'title' => 'store_access',
            ],
            [
                'id'    => '162',
                'title' => 'approval_create',
            ],
            [
                'id'    => '163',
                'title' => 'approval_edit',
            ],
            [
                'id'    => '164',
                'title' => 'approval_show',
            ],
            [
                'id'    => '165',
                'title' => 'approval_delete',
            ],
            [
                'id'    => '166',
                'title' => 'approval_access',
            ],
            [
                'id'    => '167',
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
