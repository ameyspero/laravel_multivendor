<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentGatewaysTable extends Migration
{
    public function up()
    {
        Schema::create('payment_gateways', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->nullable();
            $table->string('title');
            $table->longText('settings')->nullable();
            $table->string('is_payable')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
