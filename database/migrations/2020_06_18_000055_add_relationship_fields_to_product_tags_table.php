<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToProductTagsTable extends Migration
{
    public function up()
    {
        Schema::table('product_tags', function (Blueprint $table) {
            $table->unsignedInteger('language_id')->nullable();
            $table->foreign('language_id', 'language_fk_1467183')->references('id')->on('languages');
        });
    }
}
