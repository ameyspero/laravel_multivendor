<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTypePivotTable extends Migration
{
    public function up()
    {
        Schema::create('product_type', function (Blueprint $table) {
            $table->unsignedInteger('type_id');
            $table->foreign('type_id', 'type_id_fk_1467184')->references('id')->on('types')->onDelete('cascade');
            $table->unsignedInteger('product_id');
            $table->foreign('product_id', 'product_id_fk_1467184')->references('id')->on('products')->onDelete('cascade');
        });
    }
}
