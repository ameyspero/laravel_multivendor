<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToProductSeosTable extends Migration
{
    public function up()
    {
        Schema::table('product_seos', function (Blueprint $table) {
            $table->unsignedInteger('product_id')->nullable();
            $table->foreign('product_id', 'product_fk_1467216')->references('id')->on('products');
            $table->unsignedInteger('language_id');
            $table->foreign('language_id', 'language_fk_1467220')->references('id')->on('languages');
        });
    }
}
