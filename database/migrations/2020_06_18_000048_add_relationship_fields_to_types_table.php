<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToTypesTable extends Migration
{
    public function up()
    {
        Schema::table('types', function (Blueprint $table) {
            $table->unsignedInteger('language_id');
            $table->foreign('language_id', 'language_fk_1467185')->references('id')->on('languages');
        });
    }
}
