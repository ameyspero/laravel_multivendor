<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->longText('description')->nullable();
            $table->decimal('price', 15, 2)->nullable();
            $table->string('status')->nullable();
            $table->string('product_type')->nullable();
            $table->integer('stock_items')->nullable();
            $table->date('expire_on')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
